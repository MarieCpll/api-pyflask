from flask import Flask, jsonify, request, render_template, redirect
from flask_pymongo import pymongo

#connect BDD
CONNECTION_STRING = "mongodb+srv://dbUser:RStiaVXznPzdNLMi@cluster0-jwlck.mongodb.net/test"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('tools')
user_collection = pymongo.collection.Collection(db, 'veille')

# creating a Flask app 
app = Flask(__name__) 

@app.route('/api/v1/', methods=['GET'])
def doc():
	render_template("documentation.html")

@app.route('/api/v1/tools', methods=['POST','GET'])
def add():
	if(request.method == 'POST'):
		title = request.form["title"]
		description = request.form["description"]
		lien = request.form["lien"]
		auteur = request.form["auteur"]
		tags = request.form["tags"]
		db.veille.insert_one({"title":title, "description":description, "lien":lien, "auteur":auteur, "tags":tags})
		return redirect("/api/v1/tools")
	else:
		results = db.veille.find({})
		veilles = []
		for liste in results:
			veilles.append({'title':liste['title'], 'description':liste['description'], 'lien':liste['lien'], 'auteur':liste['auteur'], 'tags':liste['tags']})

		return jsonify({"veilles":veilles})




# driver function 
if __name__ == '__main__': 

	app.run(debug = True) 
